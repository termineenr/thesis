#include "ros/ros.h"
#include "sensor_msgs/Range.h"
#include "sensor_msgs/LaserScan.h"

void sToLCallback(const sensor_msgs::Range::ConstPtr& msg, sensor_msgs::LaserScan laserMsg, ros::Publisher pub){
    laserMsg.header = msg->header;
    //laserMsg.header.frame_id = "ShovelLaser_frame";
    laserMsg.angle_min = -(msg->field_of_view)/2;
    laserMsg.angle_max = (msg->field_of_view)/2;
    laserMsg.angle_increment = msg->field_of_view;
    laserMsg.time_increment = 0;
    laserMsg.scan_time = 0;
    laserMsg.range_min = msg->min_range;
    laserMsg.range_max = msg->max_range;
    std::vector<float> range;
    range.push_back(msg->range); 
    laserMsg.ranges = range;

    pub.publish(laserMsg);
    ros::spinOnce();
    ROS_INFO("I'm here");
}

int main(int argc, char** argv){
    ros::init(argc, argv, "scan");

    ros::NodeHandle n;

    sensor_msgs::LaserScan laserMsg;
    ros::Publisher pub = n.advertise<sensor_msgs::LaserScan>("scan",1000);
    ros::Subscriber sub = n.subscribe<sensor_msgs::Range>("/pepper_robot/sonar_front", 1000, boost::bind(sToLCallback, _1, laserMsg, pub));
    
    ros::spin();
    return 0;
}