#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>

void callback(const sensor_msgs::ImageConstPtr& rgb_msg, const sensor_msgs::ImageConstPtr& depth_msg, cv::VideoWriter vw1, cv::VideoWriter vw2){
    cv_bridge::CvImageConstPtr cv_rgb_ptr;
    cv_bridge::CvImageConstPtr cv_depth_ptr;

    cv_rgb_ptr = cv_bridge::toCvShare(rgb_msg, rgb_msg->encoding);
    cv_depth_ptr = cv_bridge::toCvShare(depth_msg, depth_msg->encoding);

    cv::Mat frame, depth;
    cv_rgb_ptr->image.copyTo(frame);
    cv_depth_ptr->image.copyTo(depth);
    cv::Mat tmp_depth(depth.rows, depth.cols, CV_8U);
    depth.convertTo(tmp_depth, CV_8U, 1.0/255.0);
    cv::imshow("prova", tmp_depth);
    cv::waitKey(1);
    vw1<<frame;
    vw2<<tmp_depth;
}

int main(int argc, char** argv){
    ros::init(argc, argv, "video_gen");
    ros::NodeHandle nh;
    
    message_filters::Subscriber<sensor_msgs::Image> rgb_sub(nh, "rgb_img", 1);
    message_filters::Subscriber<sensor_msgs::Image> depth_sub(nh, "depth_img", 1);
    message_filters::TimeSynchronizer<sensor_msgs::Image,sensor_msgs::Image> sync(rgb_sub, depth_sub, 10);
    cv::VideoWriter vw1, vw2;
    vw1.open("/home/enrico/Desktop/mappingrgb.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 15, cv::Size(960,540), true);
    vw2.open("/home/enrico/Desktop/mappingdepth.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 15, cv::Size(960,540), true);
    sync.registerCallback(boost::bind(&callback, _1, _2, vw1, vw2));
    ros::spin();
}