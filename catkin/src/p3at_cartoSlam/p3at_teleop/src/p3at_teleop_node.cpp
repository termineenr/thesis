#include <ros/ros.h>
#include "p3at_teleop/p3at_teleop_joy.h"

int main(int argc, char** argv){
    ros::init(argc, argv, "p3at_teleop_joy");

    ros::NodeHandle nh(""), nh_param("~");
    p3at_cartoSlam::p3at_teleop_joy joyTeleop(&nh, &nh_param);

    ros::spin();
}