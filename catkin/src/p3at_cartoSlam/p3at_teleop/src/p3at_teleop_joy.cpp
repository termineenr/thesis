#include "p3at_teleop/p3at_teleop_joy.h"

namespace p3at_cartoSlam
{
    struct p3at_teleop_joy::Param{
        void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

        ros::Subscriber joy_sub;
        ros::Publisher direction_pub;

        int enable_button;
        int north_but;
        int east_but;
        int south_but;
        int west_but;
        int stop_but;

        bool sent_disable_msg;
    };

    p3at_teleop_joy::p3at_teleop_joy(ros::NodeHandle* nh, ros::NodeHandle* nh_param){
        pm = new Param;

        pm->direction_pub = nh->advertise<p3at_teleop::teleop_msg>("direction", 1);
        pm->joy_sub = nh->subscribe<sensor_msgs::Joy>("joy", 1, &p3at_teleop_joy::Param::joyCallback, pm);

        nh_param->param<int>("enable_button", pm->enable_button, 0);
        nh_param->param<int>("north_but", pm->north_but, 4);
        nh_param->param<int>("east_but", pm->east_but, 5);
        nh_param->param<int>("south_but", pm->south_but, 6);
        nh_param->param<int>("west_but", pm->west_but, 7);
        nh_param->param<int>("stop_but", pm->stop_but, 2);

        ROS_INFO_NAMED("p3at_teleop_joy", "Teleop enable button %i.", pm->enable_button);

        pm->sent_disable_msg = false;
    }

    void p3at_teleop_joy::Param::joyCallback(const sensor_msgs::Joy::ConstPtr& joy){
        p3at_teleop::teleop_msg dir_msg;

        if(joy->buttons[enable_button]){
            /*if(joy->buttons[north_but]){
                dir_msg.direction = NORTH;
                ROS_INFO("North direction selected");
            }
            else if(joy->buttons[east_but]){
                dir_msg.direction = EAST;
                ROS_INFO("East direction selected");
            }
            else if(joy->buttons[south_but]){
                dir_msg.direction = SOUTH;
                ROS_INFO("South direction selected");
            }
            else if(joy->buttons[west_but]){
                dir_msg.direction = WEST;
                ROS_INFO("West direction selected");
            }*/
            if(joy->axes[1] != 0 || joy->axes[0] != 0){
                float x_intensity = joy->axes[1];
                float y_intensity = joy->axes[0];
                float direction = atan2(y_intensity, x_intensity);
                ROS_INFO("direction: %f", direction);
                dir_msg.direction = direction;
                dir_msg.enable = 1; 
            }
            else if(joy->buttons[stop_but]){
                dir_msg.enable = 0;
                ROS_INFO("Stop command");
            }
            else{
                return;
            }

            dir_msg.header.stamp = ros::Time::now();
            direction_pub.publish(dir_msg);
        }
    }
}