#include "p3at_teleop/p3at_teleop.h"

p3at_Teleop::p3at_Teleop(){
    velocity.linear.x, velocity.linear.y, velocity.linear.z = 0.0;
    velocity.angular.x, velocity.angular.y, velocity.angular.z = 0.0;
    pub = n.advertise<geometry_msgs::Twist>("cmd_vel",10);
}

void p3at_Teleop::run_teleop(){
    char c;
    bool go = true;
    initscr();
    printw("---p3at_teleop running---\n");
    printw("Use the keyboard to move the robot:\n\n");
    printw("    q w e\n    a   d\n    < z x\n\n");
    printw("Use 'p' to stop the program\n");
    while(go){
        timeout(50);
        noecho();
        c = getch();
        switch(c){
        case FORWARD:
            velocity.linear.x = 0.6;
            velocity.angular.z = 0.0;
            break;
        case FORWARD_L:
            velocity.linear.x = 0.6;
            velocity.angular.z = 0.6;
            break;
        case FORWARD_R:
            velocity.linear.x = 0.6;
            velocity.angular.z = -0.6;
            break;
        case BACKWARD:
            velocity.linear.x = -0.6;
            velocity.angular.z = 0.0;
            break;
        case BACKWARD_L:
            velocity.linear.x = -0.6;
            velocity.angular.z = 0.6;
            break;
        case BACKWARD_R:
            velocity.linear.x = -0.6;
            velocity.angular.z = -0.6;
            break;
        case TURN_L:
            velocity.linear.x = 0.0;
            velocity.angular.z = 0.6;
            break;
        case TURN_R:
            velocity.linear.x = 0.0;
            velocity.angular.z = -0.6;
            break;
        case STOP:
            go = false;
            break;
        default:
            velocity.linear.x = 0.0;
            velocity.angular.z = 0.0;
            break;
        }

        pub.publish(velocity);
        ros::spinOnce();
    }
    endwin();
    return;   
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "p3at_teleop");
    p3at_Teleop tele;

    tele.run_teleop();

    return 0;
}

