#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <math.h>
#include "p3at_teleop/teleop_msg.h"

#define DEFAULT_DIR 0
#define NORTH 1
#define EAST 2
#define SOUTH 3
#define WEST 4

namespace p3at_cartoSlam
{
    class p3at_teleop_joy{
        private:
            struct Param;
            Param* pm;
        public:
            p3at_teleop_joy(ros::NodeHandle* nh, ros::NodeHandle* nh_param);
    };
}