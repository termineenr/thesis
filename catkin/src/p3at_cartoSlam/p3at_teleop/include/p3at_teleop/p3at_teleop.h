#ifndef P3AT_TELEOP
#define P3AT_TELEOP

#include <ncurses.h>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

#define FORWARD 119
#define FORWARD_L 113
#define FORWARD_R 101
#define BACKWARD 122
#define BACKWARD_L 60
#define BACKWARD_R 120
#define TURN_L 97
#define TURN_R 100
#define STOP 112

class p3at_Teleop{
    private:
        ros::NodeHandle n;
        ros::Publisher pub;
        geometry_msgs::Twist velocity;

    public:
        p3at_Teleop();
        void run_teleop();
         
};

#endif