#include "heuristics.h"

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
typedef message_filters::sync_policies::ApproximateTime<p3at_teleop::teleop_msg, nav_msgs::Odometry> TelOdSync;
typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::LaserScan, nav_msgs::Odometry> LaserOdSync;

namespace p3at_cartoSlam
{
    struct Pose{
        float x;
        float y;
    };

    struct Orientation{
        float x;
        float y;
        float z;
    };

    struct SingleDetection{
        Pose doorPose;
        float aperture;
        time_t detectionTime;
    };

    struct heuristics::Knowledge{
        void changedDirCallback(const p3at_teleop::teleop_msg::ConstPtr& direction_msg, const nav_msgs::Odometry::ConstPtr& odom_msg);
        void laserCallback(const sensor_msgs::LaserScan::ConstPtr& laser_msg, const nav_msgs::Odometry::ConstPtr& odom_msg);
        void detectionCallback(const p3at_carto::DoorDetection::ConstPtr& detection_msg);
        std::vector<float> computeDoorsInfluence(Pose robotPose);

        ros::Subscriber detection_sub;

        message_filters::Subscriber<p3at_teleop::teleop_msg> dir_sub;
        message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub;
        message_filters::Subscriber<nav_msgs::Odometry> odom_sub;

        boost::shared_ptr<message_filters::Synchronizer<TelOdSync>> sync_do;
        
        boost::shared_ptr<message_filters::Synchronizer<LaserOdSync>> sync_lo;

        float last_human_direction;
        Pose last_human_location;
        Orientation last_human_orientation;
        bool stop = true;
        bool goalSet = false;

        float range_thresh;
        float move_distance;
        float alpha_weight, beta_weight, gamma_weight;
        float epsilon;
        float update_interval;

        bool direction_updated = false;
        time_t last_update;

        std::vector<SingleDetection> detectionList;
        float doors_distance_thresh;
    };    

    heuristics::heuristics(ros::NodeHandle* nh, ros::NodeHandle* nh_params){
        know = new Knowledge;

        nh_params->param<float>("range_thresh", know->range_thresh, 0.5);
        nh_params->param<float>("move_distance", know->move_distance, 1.0);
        nh_params->param<float>("alpha_weight", know->alpha_weight, 1);
        nh_params->param<float>("beta_weight", know->beta_weight, 10);
        nh_params->param<float>("gamma_weight", know->gamma_weight, 1);
        nh_params->param<float>("epsilon", know->epsilon, 0.01);
        nh_params->param<float>("update_interval", know->update_interval, 3.0);
        nh_params->param<float>("doors_distance_thresh", know->doors_distance_thresh, 0.7);

        know->detection_sub = nh->subscribe<p3at_carto::DoorDetection>("/door_detection", 1, &heuristics::Knowledge::detectionCallback, know);
        know->dir_sub.subscribe(*nh, "direction", 1);
        know->laser_sub.subscribe(*nh, "laser_scan", 1);
        know->odom_sub.subscribe(*nh, "pose", 1);
    
        know->sync_do.reset(new message_filters::Synchronizer<TelOdSync>(TelOdSync(5), know->dir_sub, know->odom_sub));
        know->sync_do->registerCallback(boost::bind(&heuristics::Knowledge::changedDirCallback, know, _1, _2));
        know->sync_lo.reset(new message_filters::Synchronizer<LaserOdSync>(LaserOdSync(5), know->laser_sub, know->odom_sub));
        know->sync_lo->registerCallback(boost::bind(&heuristics::Knowledge::laserCallback, know, _1, _2));
        know->last_update = time(NULL);
    }

    Orientation convert_from_quaternions(nav_msgs::Odometry odom_msg){
        geometry_msgs::Quaternion orientation = odom_msg.pose.pose.orientation;
        if(orientation.w > 1){
            float denom = sqrt(pow(orientation.x,2) + pow(orientation.y,2) + pow(orientation.z,2) + pow(orientation.w,2));
            orientation.x = (orientation.x + orientation.w)/denom;
            orientation.y = (orientation.y + orientation.w)/denom;
            orientation.z = (orientation.z + orientation.w)/denom;
            orientation.w = (orientation.w)/denom;
        }
        Orientation conversion;
        float angle = 2 * acos(orientation.w);
        float s = sqrt(1-pow(orientation.w, 2));
        if(s < 0.001){
            conversion.x = orientation.x;
            conversion.y = orientation.y;
            conversion.z = orientation.z;
        }
        else{
            conversion.x = orientation.x / s;
            conversion.y = orientation.y / s;
            conversion.z = orientation.z / s;
        }
        return conversion;
    }
    

    std::vector<float> transform_and_normalize(std::vector<float> data, float range_thresh){
        for(int i = 0; i < data.size(); i++){
            data[i] = log(data[i] + range_thresh);
        }
        
        float max_value = *(std::max_element(data.begin(), data.end()));
        for(int i = 0; i < data.size(); i++){
            data[i] = data[i] / max_value;
        }
        return data;
    }

    float rad_distance(float rad1, float human_dir, Orientation orientation){
        float rad2 = human_dir + orientation.z;
        /*switch (human_dir){
            case 1:
                rad2 = orientation.z;
                break;
            case 2:
                rad2 = orientation.z - M_PI_2;
                break;
            case 3:
                rad2 = orientation.z + M_PI;
                break;
            case 4:
                rad2 = orientation.z + M_PI_2;
                break;        
            default:
                rad2 = 0;
                break;
        }*/
        return std::abs(rad1 - rad2);
    }

    Pose compute_future_pos(Pose curPos, float angle, float moveDistance){
        Pose future_pos;
        future_pos.x = curPos.x + moveDistance*cos(angle);
        future_pos.y = curPos.y + moveDistance*sin(angle);
        return future_pos;
    }

    float compute_rel_distance(Pose startPos, Pose curPos, Pose futPos, int direction){
        float distance, abs_distance, rel_distance;

        if(direction == 1 || direction == 3){
            distance = abs(startPos.y - curPos.y);
            abs_distance = abs(startPos.y - futPos.y);
            rel_distance = abs_distance - distance;
        }
        else{
            distance = abs(startPos.x - curPos.x);
            abs_distance = abs(startPos.x - futPos.x);
            rel_distance = abs_distance - distance;
        }

        return rel_distance;
    }

    std::vector<float> heuristics::Knowledge::computeDoorsInfluence(Pose robotPose){
        std::vector<float> doorWeights;
        for(int i = 0; i < detectionList.size(); i++){
            SingleDetection door = detectionList[i];
            float x = door.doorPose.x - robotPose.x;
            float y = door.doorPose.y - robotPose.y;
            float angleDoorRobot = atan2(y, x);

            Orientation zeroOrientation;
            zeroOrientation.z = 0;
            float angleDoorDir = rad_distance(angleDoorRobot, last_human_direction, zeroOrientation);
            if(angleDoorDir < 45){
                doorWeights.push_back(angleDoorDir);
            }
        }

        return doorWeights;
    }
    
    void heuristics::Knowledge::changedDirCallback(const p3at_teleop::teleop_msg::ConstPtr& direction_msg, const nav_msgs::Odometry::ConstPtr& odom_msg){
        ROS_INFO("Direction received");
        last_human_direction = direction_msg->direction;
        last_human_location.x = odom_msg->pose.pose.position.x;
        last_human_location.y = odom_msg->pose.pose.position.y;
        last_human_orientation = convert_from_quaternions(*odom_msg);

        MoveBaseClient mbc("move_base", true);
        while(!mbc.waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for the move_base action server to come up");
        }

        if(direction_msg->enable == 0){
            mbc.cancelAllGoals();
            stop = true;
            direction_updated = false;
        }
        else{
            if(std::abs(last_human_direction) > 2.36){
                mbc.cancelAllGoals();
                move_base_msgs::MoveBaseGoal goal;

                goal.target_pose.header.frame_id = "base_link";
                goal.target_pose.header.stamp = ros::Time::now();
                goal.target_pose.pose.orientation.z = -M_PI;
                goal.target_pose.pose.orientation.w = 1;

                mbc.sendGoal(goal);
                mbc.waitForResult();
            }
            stop = false;
            goalSet = true;
            direction_updated = true;
        }
    }

    void heuristics::Knowledge::laserCallback(const sensor_msgs::LaserScan::ConstPtr& laser_msg, const nav_msgs::Odometry::ConstPtr& odom_msg){
        //ROS_INFO("Laser data recceived");
        MoveBaseClient mbc("move_base", true);
        while(!mbc.waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for the move_base action server to come up");
        }
        if(stop){
            if(goalSet){
                mbc.cancelAllGoals();
                goalSet = false;
            }
            return;
        }
        if(time(NULL) - last_update < update_interval && !direction_updated){
            return;
        }

        direction_updated = false;

        float norm_value = laser_msg->range_max;
        float angle_min = laser_msg->angle_min;
        float angle_max = laser_msg->angle_max;
        float angle_increment = laser_msg->angle_increment;
        
        std::vector<float> data = transform_and_normalize(laser_msg->ranges, range_thresh);
        
        std::vector<float> heur_values(data.size());
        float max_heur = 0;
        float max_range = *(std::max_element(laser_msg->ranges.begin(), laser_msg->ranges.end()));
        Pose current_pose;
        current_pose.x = odom_msg->pose.pose.position.x;
        current_pose.y = odom_msg->pose.pose.position.y;
        ROS_INFO("Doors computing");
        std::vector<float> doorsInfluence = computeDoorsInfluence(current_pose);
        ROS_INFO("Doors computed");
        Pose selected_pose;
        Orientation selected_orientation;
        Orientation rotation = convert_from_quaternions(*odom_msg);

        for(int i = 0; i < data.size(); i++){
            float obs_distance = data[i];
            
            float angle = rotation.z + angle_min + angle_increment*i;
            float angular_distance = rad_distance(angle, last_human_direction, last_human_orientation);
            //printf("ang_dist: %f---angle: %f\n", angular_distance, angle);

            Pose future_pose = compute_future_pos(current_pose, angle, laser_msg->ranges[i]*0.8);

            float rel_distance = compute_rel_distance(last_human_location, current_pose, future_pose, last_human_direction);
            
            float heur_value = ((alpha_weight*obs_distance) + (beta_weight*(epsilon*(1/(angular_distance+epsilon)))) + (gamma_weight*(exp(rel_distance)/exp(max_range*0.8))))/(alpha_weight+beta_weight+gamma_weight);

            for(int d = 0; d < doorsInfluence.size(); d++){
                float doorWeight = (1/sqrt(2*M_PI)) * exp(-pow(doorsInfluence[d]-angle,2)/2);
                heur_value = heur_value + doorWeight;
            }
            //ROS_INFO("alpha: %f\nbeta: %f\ngamma: %f\n",alpha_weight*obs_distance,(beta_weight*(epsilon*(1/(angular_distance+epsilon)))), (gamma_weight*(exp(rel_distance)/exp(move_distance))));
            //ROS_INFO("COMPARISON: %f vs %f", max_heur, heur_value);
            if(heur_value > max_heur){
                max_heur = heur_value;
                selected_orientation.z = angle_min + angle_increment*i;
                ROS_INFO("MAX HEUR %f at angle %f", max_heur, selected_orientation.z);
                ROS_INFO("apha: %f----beta: %f----gamma: %f", alpha_weight*obs_distance,(beta_weight*(epsilon*(1/(angular_distance+epsilon)))), (gamma_weight*(exp(rel_distance)/exp(max_range*0.8))));
                selected_pose.x = laser_msg->ranges[i]*0.8 * cos(selected_orientation.z);
                selected_pose.y = laser_msg->ranges[i]*0.8 * sin(selected_orientation.z);
            }
        }
        ROS_INFO("lhd: %f---lho: %f---rotation: %f", last_human_direction, last_human_orientation.z, rotation.z);
        move_base_msgs::MoveBaseGoal goal;

        goal.target_pose.header.frame_id = "base_link";
        goal.target_pose.header.stamp = ros::Time::now();

        goal.target_pose.pose.position.x = selected_pose.x;
        goal.target_pose.pose.position.y = selected_pose.y;
        ROS_INFO("angle: %f", selected_orientation.z);
        
        tf2::Quaternion new_orientation;
        new_orientation.setRPY(0,0,selected_orientation.z);

        goal.target_pose.pose.orientation.x = (double)new_orientation.x();
        goal.target_pose.pose.orientation.y = (double)new_orientation.y();
        goal.target_pose.pose.orientation.z = (double)new_orientation.z();
        goal.target_pose.pose.orientation.w = (double)new_orientation.w();
        ROS_INFO("%f", goal.target_pose.pose.position.x);
        ROS_INFO("%f", goal.target_pose.pose.position.y);
        ROS_INFO("%f", goal.target_pose.pose.orientation.z);
        //sleep(1000);
        mbc.sendGoal(goal);
        last_update = time(NULL);
        //mbc.waitForResult();
        ROS_INFO("enrico2");

    }

    void heuristics::Knowledge::detectionCallback(const p3at_carto::DoorDetection::ConstPtr& detection_msg){
        nav_msgs::Odometry tmpPose = detection_msg->odom;
        Orientation rotation = convert_from_quaternions(tmpPose);
        std::vector<p3at_carto::DoorInfo> tmpDetectionList = detection_msg->detectionList;
        for(int i = 0; i < tmpDetectionList.size(); i++){
            
            p3at_carto::DoorInfo thisDoor = tmpDetectionList[i];
            float thisDoorAngle = atan2(thisDoor.centerX, thisDoor.centerZ);
            float thisDoorX = tmpPose.pose.pose.position.x + thisDoor.centerZ * cos(rotation.z+thisDoorAngle);
            float thisDoorY = tmpPose.pose.pose.position.y + thisDoor.centerX * cos(rotation.z+thisDoorAngle);
            float aperture = thisDoor.aperture;
            bool duplicate = false;
            
            for(int j = 0; j < detectionList.size(); j++){
                SingleDetection comparedDoor = detectionList[j];
                float doorsDistance = sqrt(pow(comparedDoor.doorPose.x-thisDoorX, 2) + pow(comparedDoor.doorPose.y-thisDoorY, 2));
                if(doorsDistance < doors_distance_thresh){
                    comparedDoor.doorPose.x = thisDoorX;
                    comparedDoor.doorPose.y = thisDoorY;
                    comparedDoor.aperture = aperture;
                    time(&comparedDoor.detectionTime);
                    duplicate = true;
                    break;
                }
            }

            if(!duplicate){
                ROS_INFO("New Door");
                SingleDetection newDoor;
                newDoor.doorPose.x = thisDoorX;
                newDoor.doorPose.y = thisDoorY;
                newDoor.aperture = aperture;
                time(&newDoor.detectionTime);
                detectionList.push_back(newDoor);
            }
        }
    }
}