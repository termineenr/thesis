#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <nav_msgs/Odometry.h>
#include <p3at_carto/DoorInfo.h>
#include <p3at_carto/DoorDetection.h>
#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include "yoloTrack.h"

namespace p3at_cartoSlam{
    class door_detector{
        private:
            struct detectInfo;
            detectInfo* info;
        public:
            door_detector(ros::NodeHandle* nh, ros::NodeHandle* nh_params);
    };
    
}