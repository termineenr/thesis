#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Int32.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <p3at_teleop/teleop_msg.h>
#include <p3at_carto/DoorDetection.h>
#include <tf/transform_listener.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <move_base_msgs/MoveBaseActionGoal.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/action_client.h>
#include <actionlib/client/client_helpers.h>
#include <cmath>
#include <mutex>
#include <algorithm>
#include <string>

namespace p3at_cartoSlam
{
    class heuristics{
        public:
            struct Knowledge;
            heuristics(ros::NodeHandle* nh, ros::NodeHandle* nh_params);
        private:
            Knowledge* know;
    };
}