#ifndef DOOR
#define DOOR

#include <opencv2/core.hpp>
#include <opencv2/tracking/tracker.hpp>

using namespace cv;
using namespace std;

class Door{
    private:
        Ptr<Tracker> doorTracker;
        Rect2d doorBbox;
        Mat curFrame;
    
    public:
        int numDetection = 1;
        int numFrames = 1;
        bool toVisualize = true;
        bool apertureComputed = false;
        Door(Mat frame, Rect2d initialBbox){
            curFrame = frame;
            doorBbox = initialBbox;
            doorTracker = TrackerMedianFlow::create();
            //cout<<frame.rows<<" "<<frame.cols<<endl;
            //cout<<doorBbox<<endl;
            doorTracker->init(curFrame, doorBbox);
            //cout<<"add"<<endl;
        };
        Rect2d getBbox(){ return doorBbox; };
        void setBbox(Rect2d box){ doorBbox = box; };
        void setFrame(Mat frame){
            curFrame = frame;
            numFrames++;
        };
        void updateTracker(){ toVisualize = doorTracker->update(curFrame, doorBbox); };
        void substituteTracker(){
            doorTracker = TrackerMedianFlow::create();
            doorTracker->init(curFrame, doorBbox);
        };
};
#endif