#include <opencv2/core.hpp>
#include <opencv2/tracking/tracker.hpp>
#include <vector>
#include <math.h>
#include <iostream>
#include "Door2.h"

class yoloTrack{
    public:
        std::vector<Door> trackDoors;
        yoloTrack();
        void setCurFrame(cv::Mat frame);
        void trackAll();
        int isInList(Door door);
};