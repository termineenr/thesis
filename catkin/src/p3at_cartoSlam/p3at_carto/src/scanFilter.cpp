#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <vector>

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg, ros::Publisher pub){
    sensor_msgs::LaserScan filteredMsg;
    filteredMsg.header = msg->header;
    filteredMsg.angle_increment = msg->angle_increment;
    filteredMsg.time_increment = msg->time_increment;
    filteredMsg.scan_time = msg->scan_time;
    filteredMsg.angle_max = msg->angle_max - msg->angle_increment * 20;
    filteredMsg.angle_min = msg->angle_min + msg->angle_increment * 20;
    filteredMsg.range_max = msg->range_max;
    filteredMsg.range_min = msg->range_min;
    std::vector<float> ranges;

    for(int i = 20; i < msg->ranges.size()-20; i++){
        ranges.push_back(msg->ranges[i]);
    }
    filteredMsg.ranges = ranges;
    
    pub.publish(filteredMsg);
}

int main(int argc, char** argv){
    ros::init(argc, argv, "scan_filter");
    ros::NodeHandle nh("");
    ros::Publisher pub = nh.advertise<sensor_msgs::LaserScan>("laser_scan", 10);
    ros::Subscriber sub = nh.subscribe<sensor_msgs::LaserScan>("scan", 10, boost::bind(scanCallback, _1, pub));
    ros::spin();
    return 0;
}