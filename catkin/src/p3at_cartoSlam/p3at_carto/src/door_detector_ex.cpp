#include "door_detector.h"

int main(int argc, char** argv){
    ros::init(argc, argv, "door_detector");

    ros::NodeHandle nh(""), nh_params("~");

    p3at_cartoSlam::door_detector dd_node(&nh, &nh_params);

    ros::Rate r(2);
    while(ros::ok()){
        ros::spinOnce();
        r.sleep();
    }
    
}