#include "heuristics.h"

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
typedef message_filters::sync_policies::ApproximateTime<p3at_teleop::teleop_msg, nav_msgs::Odometry> TelOdSync;
typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::LaserScan, nav_msgs::Odometry> LaserOdSync;

namespace p3at_cartoSlam
{
    struct Pose{
        float x;
        float y;
    };

    struct Orientation{
        float x;
        float y;
        float z;
    };

    struct SingleDetection{
        Pose doorPose;
        float aperture;
        time_t detectionTime;
    };

    struct heuristics::Knowledge{
        void changedDirCallback(const p3at_teleop::teleop_msg::ConstPtr& direction_msg);
        void laserCallback(const sensor_msgs::LaserScan::ConstPtr& laser_msg);
        void detectionCallback(const p3at_carto::DoorDetection::ConstPtr& detection_msg);
        void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& map);
        void goalStatusCallback(const actionlib_msgs::GoalStatusArray::ConstPtr& status);
        //void goalStatusCallback()
        std::vector<float> computeDoorsInfluence(Pose robotPose, Orientation robotOrientation);

        ros::Subscriber detection_sub;
        ros::Subscriber dir_sub;
        ros::Subscriber laser_sub;
        ros::Subscriber map_sub;
        ros::Subscriber goal_status_sub;

        float last_human_direction;
        Pose last_human_location;
        Orientation last_human_orientation;

        std::mutex stop_lock;
        bool stop = true;
        
        std::mutex goalSet_lock;
        bool goalSet = false;
        
        nav_msgs::OccupancyGrid last_map;
        
        std::mutex dir_up_lock;
        bool direction_updated = false;
        
        time_t last_update;

        std::vector<SingleDetection> detectionList;
        float doors_distance_thresh;

        float range_thresh;
        float move_distance;
        float alpha_weight, beta_weight, gamma_weight, delta_weight;
        float epsilon;
        float range_percentage;
        float update_interval;

        
    };    

    heuristics::heuristics(ros::NodeHandle* nh, ros::NodeHandle* nh_params){
        know = new Knowledge;

        nh_params->param<float>("range_thresh", know->range_thresh, 0.5);
        nh_params->param<float>("move_distance", know->move_distance, 1.0);
        nh_params->param<float>("alpha_weight", know->alpha_weight, 1);
        nh_params->param<float>("beta_weight", know->beta_weight, 10);
        nh_params->param<float>("gamma_weight", know->gamma_weight, 1);
        nh_params->param<float>("delta_weight", know->delta_weight, 1);
        nh_params->param<float>("epsilon", know->epsilon, 0.01);
        nh_params->param<float>("range_percentage", know->range_percentage, 0.2);
        nh_params->param<float>("update_interval", know->update_interval, 3.0);
        nh_params->param<float>("doors_distance_thresh", know->doors_distance_thresh, 0.7);

        know->detection_sub = nh->subscribe<p3at_carto::DoorDetection>("/door_detection", 1, &heuristics::Knowledge::detectionCallback, know);
        know->dir_sub = nh->subscribe<p3at_teleop::teleop_msg>("/direction", 1, &heuristics::Knowledge::changedDirCallback, know);
        know->laser_sub = nh->subscribe<sensor_msgs::LaserScan>("/laser_scan", 1, &heuristics::Knowledge::laserCallback, know);
        know->map_sub = nh->subscribe<nav_msgs::OccupancyGrid>("/map", 1, &heuristics::Knowledge::mapCallback, know);
        know->goal_status_sub = nh->subscribe<actionlib_msgs::GoalStatusArray>("/move_base/status", 1, &heuristics::Knowledge::goalStatusCallback, know);
    
        know->last_update = time(NULL);
    }

    Orientation convert_from_quaternions(nav_msgs::Odometry odom_msg){
        geometry_msgs::Quaternion orientation = odom_msg.pose.pose.orientation;
        ROS_INFO("X-Y-Z-W: %f,%f,%f,%f", orientation.x, orientation.y, orientation.z, orientation.w);
        if(orientation.w > 1){
            float denom = sqrt(pow(orientation.x,2) + pow(orientation.y,2) + pow(orientation.z,2) + pow(orientation.w,2));
            orientation.x = (orientation.x + orientation.w)/denom;
            orientation.y = (orientation.y + orientation.w)/denom;
            orientation.z = (orientation.z + orientation.w)/denom;
            orientation.w = (orientation.w)/denom;
        }
        Orientation conversion;
        float angle = 2 * acos(orientation.w);
        float s = sqrt(1-pow(orientation.w, 2));
        ROS_INFO("S: %f", s);
        if(s < 0.001){
            conversion.x = orientation.x;
            conversion.y = orientation.y;
            conversion.z = orientation.z;
        }
        else{
            conversion.x = orientation.x / s;
            conversion.y = orientation.y / s;
            conversion.z = orientation.z / s;
        }
        return conversion;
    }
    

    std::vector<float> transform_and_normalize(std::vector<float> data, float range_thresh){
        for(int i = 0; i < data.size(); i++){
            data[i] = log(data[i] + range_thresh);
        }
        
        float max_value = *(std::max_element(data.begin(), data.end()));
        for(int i = 0; i < data.size(); i++){
            data[i] = data[i] / max_value;
        }
        return data;
    }

    float rad_distance(float rad1, float human_dir, Orientation orientation){
        float rad2 = human_dir + orientation.z;
        
        /*if(rad1 > M_PI){
            rad1 = -(2*M_PI - rad1);
            ROS_INFO("caso 1");
        }
        else if (rad1 < -M_PI){
            rad1 = 2*M_PI + rad1;
            ROS_INFO("caso 2");
        }
        
        if(rad2 > M_PI){
            rad2 = -(2*M_PI - rad2);
            ROS_INFO("caso 3");
        }
        else if (rad2 < -M_PI){
            rad2 = 2*M_PI + rad2;
            ROS_INFO("caso 4");
        }*/
        
        
        /*switch (human_dir){
            case 1:
                rad2 = orientation.z;
                break;
            case 2:
                rad2 = orientation.z - M_PI_2;
                break;
            case 3:
                rad2 = orientation.z + M_PI;
                break;
            case 4:
                rad2 = orientation.z + M_PI_2;
                break;        
            default:
                rad2 = 0;
                break;
        }*/
        float angle_dif = std::abs(rad1 - rad2);
        /*if(angle_dif > M_PI){
            angle_dif = std::abs(2*M_PI - angle_dif);
        }*/
        return angle_dif;
    }

    Pose compute_future_pos(Pose curPos, float angle, float moveDistance){
        Pose future_pos;
        future_pos.x = curPos.x + moveDistance*cos(angle);
        future_pos.y = curPos.y + moveDistance*sin(angle);
        return future_pos;
    }

    float compute_rel_distance(Pose startPos, Pose curPos, Pose futPos, float direction){
        float start_cur_distance = sqrt(pow(startPos.x-curPos.x, 2) + pow(startPos.y-curPos.y, 2));
        float start_fut_distance = sqrt(pow(startPos.x-futPos.x, 2) + pow(startPos.y-futPos.y, 2));

        float start_cur_angle = atan2(curPos.y-startPos.y, curPos.x-startPos.x);
        float start_fut_angle = atan2(futPos.y-startPos.y, futPos.x-startPos.x);

        float cur_direction_distance = start_cur_distance * sin(start_cur_angle-direction);
        float fut_direction_distance = start_fut_distance * sin(start_fut_angle-direction);

        return (fut_direction_distance-cur_direction_distance);
    }

    std::vector<float> heuristics::Knowledge::computeDoorsInfluence(Pose robotPose, Orientation robotOrientation){
        tf::TransformListener listener;
        tf::StampedTransform map_st;
        bool tf_found = false;
        while(!tf_found){
            try{
                listener.lookupTransform("/base_link", "/map", ros::Time(0), map_st);
                tf_found = true;
            }
            catch(tf::TransformException& ex){}
        }

        std::vector<float> doorWeights;

        for(int i = 0; i < detectionList.size(); i++){
            SingleDetection door = detectionList[i];

            geometry_msgs::PointStamped door_map;
            door_map.header.frame_id = "/map";
            door_map.header.stamp = ros::Time();
            door_map.point.x = door.doorPose.x;
            door_map.point.y = door.doorPose.y;
            door_map.point.z = 0;

            geometry_msgs::PointStamped door_robot;
            door_robot.header.frame_id = "/base_link";
            listener.transformPoint("/base_link", door_map, door_robot);

            //float x = door_robot.point.x - robotPose.x;
            //float y = door_robot.point.y - robotPose.y;
            ROS_INFO("X Y: %f---%f", door_robot.point.x,door_robot.point.y);
            float angleDoor = atan2(door_robot.point.y, door_robot.point.x);
            ROS_INFO("ANGLE: %f", angleDoor);

            Orientation zeroOrientation;
            zeroOrientation.z = 0;
            float angleDoorDir = std::abs((angleDoor + robotOrientation.z) - (last_human_direction + last_human_orientation.z));
            ROS_INFO("Door dir: %f", angleDoorDir);
            if(angleDoorDir < 1.04){
                //float angleDoorRobot = robotOrientation.z - angleDoor;
                doorWeights.push_back(angleDoor);
                ROS_INFO("Door angle: %f", angleDoor);
            }
        }

        return doorWeights;
    }

    Pose computeValidPosition(int goal_col, int goal_row, float goal_x, float goal_y, nav_msgs::OccupancyGrid& map){
        ROS_INFO("The starting goal position is: (%f,%f).", goal_x, goal_y);
        int occupancy_thresh = 15;
        std::vector<int8_t> grid = map.data;
        int grid_width = map.info.width;
        int grid_heigth = map.info.height;
        float grid_resolution = map.info.resolution;

        Pose validPosition;

        if(grid[goal_row*grid_width+goal_col] <= occupancy_thresh && grid[goal_row*grid_width+goal_col] != -1){
            validPosition.x = goal_x;
            validPosition.y = goal_y;
            ROS_INFO("The goal position is already valid.");
        }
        else{
            int level = 1;
            
            while(true){
                int i = -level;
                int j = -level;

                for(j; j < level+1; j++){
                    try{
                        int grid_value = grid[(goal_row+i) * grid_width + (j+goal_col)];
                        if(grid_value <= occupancy_thresh && grid_value != -1){
                            float valid_goal_x = goal_x + j * grid_resolution;
                            float valid_goal_y = goal_y + i * grid_resolution;
                            validPosition.x = valid_goal_x;
                            validPosition.y = valid_goal_y;
                            ROS_INFO("The goal position now is: (%f,%f).", validPosition.x, validPosition.y);
                            return validPosition;
                        }
                    }
                    catch(const std::out_of_range& oor){}
                }
                
                j = level;
                for(i = -level+1; i < level+1; i++){
                    try{
                        int grid_value = grid[(goal_row+i) * grid_width + (j+goal_col)];
                        if(grid_value <= occupancy_thresh && grid_value != -1){
                            float valid_goal_x = goal_x + j * grid_resolution;
                            float valid_goal_y = goal_y + i * grid_resolution;
                            validPosition.x = valid_goal_x;
                            validPosition.y = valid_goal_y;
                            ROS_INFO("The goal position now is: (%f,%f).", validPosition.x, validPosition.y);
                            return validPosition;
                        }
                    }
                    catch(const std::out_of_range& oor){}
                }

                i = level;
                for(j = level-1; j > -level-1; j--){
                    try{
                        int grid_value = grid[(goal_row+i) * grid_width + (j+goal_col)];
                        if(grid_value <= occupancy_thresh && grid_value != -1){
                            float valid_goal_x = goal_x + j * grid_resolution;
                            float valid_goal_y = goal_y + i * grid_resolution;
                            validPosition.x = valid_goal_x;
                            validPosition.y = valid_goal_y;
                            ROS_INFO("The goal position now is: (%f,%f).", validPosition.x, validPosition.y);
                            return validPosition;
                        }
                    }
                    catch(const std::out_of_range& oor){}
                }

                j = -level;
                for(i = level-1; i > -level; i--){
                    try{
                        int grid_value = grid[(goal_row+i) * grid_width + (j+goal_col)];
                        if(grid_value <= occupancy_thresh && grid_value != -1){
                            float valid_goal_x = goal_x + j * grid_resolution;
                            float valid_goal_y = goal_y + i * grid_resolution;
                            validPosition.x = valid_goal_x;
                            validPosition.y = valid_goal_y;
                            ROS_INFO("The goal position now is: (%f,%f).", validPosition.x, validPosition.y);
                            return validPosition;
                        }
                    }
                    catch(const std::out_of_range& oor){}
                }

                level++;
            }
        }

        return validPosition;
    }

    void heuristics::Knowledge::goalStatusCallback(const actionlib_msgs::GoalStatusArray::ConstPtr& status){
        if(status->status_list.empty())
            return;

        int8_t cur_status = status->status_list[status->status_list.size()-1].status;
        //ROS_INFO("Status: %d", cur_status);

        MoveBaseClient mbc("move_base", true);
        while(!mbc.waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for the move_base action server to come up");
        }
        bool lk = false;

        if(cur_status == actionlib_msgs::GoalStatus::REJECTED){
            while(lk != true){
                lk = dir_up_lock.try_lock();
                //std::cout<<"LOCK"<<std::endl;

            }
            
            direction_updated = true;
            ROS_INFO("Goal rejected: recomputing goal.");
            mbc.cancelAllGoals();
            
            dir_up_lock.unlock();
        }
        else if (cur_status == actionlib_msgs::GoalStatus::ABORTED){
            while(lk != true){
                lk = dir_up_lock.try_lock();
                //std::cout<<"LOCK"<<std::endl;

            }
            
            direction_updated = true;
            ROS_INFO("Goal aborted: recomputing goal.");
            mbc.cancelAllGoals();

            dir_up_lock.unlock();
        }
        else if(cur_status == actionlib_msgs::GoalStatus::SUCCEEDED){
            while(lk != true){
                lk = dir_up_lock.try_lock();
                //std::cout<<"LOCK"<<std::endl;

            }
            
            direction_updated = true;
            ROS_INFO("Goal reached: computing new goal.");

            dir_up_lock.unlock();
        }
        
        
    }
    
    void heuristics::Knowledge::changedDirCallback(const p3at_teleop::teleop_msg::ConstPtr& direction_msg){
        ROS_INFO("Direction received");
        
        tf::TransformListener listener;
        bool tf_found = false;
        tf::StampedTransform odom;
        while(!tf_found){
            try{
                listener.lookupTransform("/base_link", "/odom", ros::Time(0), odom);
                tf_found = true;
            }
            catch(tf::TransformException& ex){}
        }
        
        tf::Vector3 odom_trans = odom.getOrigin();
        tf::Quaternion odom_rot = odom.getRotation();

        last_human_direction = direction_msg->direction;
        last_human_location.x = odom_trans.getX();
        last_human_location.y = odom_trans.getY();
        
        tf::Matrix3x3 m(odom_rot);
        double roll, pitch, yaw;
        m.getRPY(roll, pitch, yaw);
        last_human_orientation.z = yaw;

        ROS_INFO("TRANS: (%f,%f)---ROT: (%f)", last_human_location.x, last_human_location.y, yaw);

        MoveBaseClient mbc("move_base", true);
        while(!mbc.waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for the move_base action server to come up");
        }

        int lk = -2;

        if(direction_msg->enable == 0){
            mbc.cancelAllGoals();
            
            while(lk != -1){
                lk = std::try_lock(stop_lock, dir_up_lock);
                std::cout<<"LOCK"<<std::endl;
            }

            stop = true;
            direction_updated = false;

            stop_lock.unlock();
            dir_up_lock.unlock();
        }
        else if(std::abs(last_human_direction) > 2.36){
            mbc.cancelAllGoals();
            move_base_msgs::MoveBaseGoal goal;

            goal.target_pose.header.frame_id = "base_link";
            goal.target_pose.header.stamp = ros::Time::now();
            goal.target_pose.pose.orientation.z = M_PI;
            goal.target_pose.pose.orientation.w = 1;

            mbc.sendGoal(goal);
            mbc.waitForResult();

            while(lk != -1){
                lk = std::try_lock(stop_lock, dir_up_lock);
                std::cout<<"LOCK"<<std::endl;
            }

            stop = true;
            direction_updated = false;

            stop_lock.unlock();
            dir_up_lock.unlock();
        }
        else{
            while(lk != -1){
                lk = std::try_lock(stop_lock, dir_up_lock, goalSet_lock);
                std::cout<<"LOCK"<<std::endl;
            }

            stop = false;
            goalSet = true;
            direction_updated = true;

            stop_lock.unlock();
            dir_up_lock.unlock();
            goalSet_lock.unlock();
        }
        
    }

    void heuristics::Knowledge::laserCallback(const sensor_msgs::LaserScan::ConstPtr& laser_msg){
        //ROS_INFO("Laser data recceived");
        MoveBaseClient mbc("move_base", true);
        while(!mbc.waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for the move_base action server to come up");
        }
        if(stop){
            if(goalSet){
                mbc.cancelAllGoals();
                goalSet = false;
            }
            return;
        }
        if(time(NULL) - last_update < update_interval && !direction_updated){
            return;
        }

        direction_updated = false;

        tf::TransformListener listener;
        bool tf_found = false;
        tf::StampedTransform odom;
        tf::StampedTransform map_tf;

        while(!tf_found){
            try{
                listener.lookupTransform("/base_link", "/odom", ros::Time(0), odom);
                tf_found = true;
            }
            catch(tf::TransformException& ex){}
        }

        /*tf_found = false;
        while(!tf_found){
            try{
                listener.lookupTransform("/base_link", "/map", ros::Time(0), map_tf);
                tf_found = true;
            }
            catch(tf::TransformException& ex){}
        }*/

        tf::Vector3 odom_trans = odom.getOrigin();
        tf::Quaternion odom_rot = odom.getRotation();

        float norm_value = laser_msg->range_max;
        float angle_min = laser_msg->angle_min;
        float angle_max = laser_msg->angle_max;
        float angle_increment = laser_msg->angle_increment;
        
        std::vector<float> data = transform_and_normalize(laser_msg->ranges, range_thresh);
        
        float max_heur = 0;
        float max_range = *(std::max_element(laser_msg->ranges.begin(), laser_msg->ranges.end()));
        
        Pose current_pose;
        current_pose.x = odom_trans.getX();
        current_pose.y = odom_trans.getY();

        tf::Matrix3x3 m(odom_rot);
        Orientation rotation;// = convert_from_quaternions(fake_odom);
        double roll, pitch, yaw;
        m.getRPY(roll, pitch, yaw);
        rotation.z = yaw; 
        //ROS_INFO("QUATERNION: (%f,%f,%f,%f)", odom_rot.getX(), odom_rot.getY(), odom_rot.getZ(), odom_rot.getW());
        /*rotation.x = odom_rot.getX();
        rotation.y = odom_rot.getY();
        rotation.z = odom_rot.getZ();*/

        Pose selected_pose;
        Orientation selected_orientation;
        float range_value;
        
        std::vector<float> doorsInfluence = computeDoorsInfluence(current_pose, rotation);

        for(int i = 0; i < data.size(); i++){
            float obs_distance = data[i];
            
            float angle = rotation.z + angle_min + angle_increment*i;
            float angular_distance = rad_distance(angle, last_human_direction, last_human_orientation);

            //if(angular_distance < 50){
                Pose future_pose = compute_future_pos(current_pose, angle, laser_msg->ranges[i]*range_percentage);

                float rel_distance = compute_rel_distance(last_human_location, current_pose, future_pose, last_human_direction+last_human_orientation.z);
                
                float heur_value = ((alpha_weight*obs_distance) + (beta_weight*(epsilon*(1/(angular_distance+epsilon)))) + (gamma_weight * (rel_distance/std::abs(rel_distance))* (1-exp(-pow(rel_distance,2)/(2*pow(0.5,2))))))/(alpha_weight+beta_weight+gamma_weight);

                for(int d = 0; d < doorsInfluence.size(); d++){
                    float doorWeight = (1/sqrt(2*M_PI*0.05)) * exp(-pow(doorsInfluence[d]-(angle_min+angle_increment*i),2)/(2*0.05));
                    heur_value = heur_value + delta_weight * doorWeight;
                }
                //ROS_INFO("alpha: %f\nbeta: %f\ngamma: %f\n",alpha_weight*obs_distance,(beta_weight*(epsilon*(1/(angular_distance+epsilon)))), (gamma_weight*(exp(rel_distance)/exp(move_distance))));
                //ROS_INFO("COMPARISON: %f vs %f", max_heur, heur_value);
                if(heur_value > max_heur){
                    max_heur = heur_value;
                    selected_orientation.z = angle_min + angle_increment*i;
                    //ROS_INFO("LASER ANGLE-DISTANCE VALUE: (%f,%f,%f), %f", rotation.z, selected_orientation.z, angle, last_human_direction+last_human_orientation.z);
                    //ROS_INFO("MAX HEUR %f at angle %f", max_heur, selected_orientation.z);
                    //ROS_INFO("apha: %f----beta: %f----gamma: %f", alpha_weight*obs_distance,(beta_weight*(epsilon*(1/(angular_distance+epsilon)))), (gamma_weight * (rel_distance/std::abs(rel_distance))* (1-exp(-pow(rel_distance,2)/(2*pow(0.5,2))))));
                    //float doorWeight = (1/sqrt(2*M_PI*0.05)) * exp(-pow(doorsInfluence[0]-(angle_min+angle_increment*i),2)/(Z2*0.05));
                    //ROS_INFO("INFLUENCE: %f", doorWeight);
                    range_value = laser_msg->ranges[i]*range_percentage;
                    selected_pose.x = laser_msg->ranges[i]*range_percentage * cos(selected_orientation.z);
                    selected_pose.y = laser_msg->ranges[i]*range_percentage * sin(selected_orientation.z);
                }
            //}
            
        }

        geometry_msgs::PointStamped robot_goal;
        robot_goal.header.frame_id = "/base_link";
        robot_goal.header.stamp = ros::Time();
        robot_goal.point.x = selected_pose.x;
        robot_goal.point.y = selected_pose.y;
        robot_goal.point.z = 0;

        geometry_msgs::PointStamped map_goal;
        map_goal.header.frame_id = "/map";
        listener.transformPoint("/map", robot_goal, map_goal);

        //tf::Vector3 map_trans = map_tf.getOrigin();
        //tf::Quaternion map_rot = map_tf.getRotation();
        
        tf::Vector3 goal_coord(map_goal.point.x, map_goal.point.y, 0);
        
        tf::Vector3 map_origin(last_map.info.origin.position.x, last_map.info.origin.position.y, last_map.info.origin.position.z);

        tf::Vector3 goal_dist_meters = goal_coord - map_origin;
        tf::Vector3 grid_goal_coord = goal_dist_meters / last_map.info.resolution;

        int goal_column = ceil(grid_goal_coord.getX());
        int goal_row = ceil(grid_goal_coord.getY());

        Pose good_map_goal = computeValidPosition(goal_column, goal_row, goal_coord.getX(), goal_coord.getY(), last_map);

        geometry_msgs::PointStamped stamped_good_mg;
        stamped_good_mg.header.frame_id = "/map";
        stamped_good_mg.header.stamp = ros::Time();
        stamped_good_mg.point.x = good_map_goal.x;
        stamped_good_mg.point.y = good_map_goal.y;
        stamped_good_mg.point.z = 0;

        geometry_msgs::PointStamped stamped_good_rg;
        stamped_good_rg.header.frame_id = "/base_link";
        listener.transformPoint("/base_link", stamped_good_mg, stamped_good_rg);

        //ROS_INFO("lhd: %f---lho: %f---rotation: %f", last_human_direction, last_human_orientation.z, rotation.z);
        move_base_msgs::MoveBaseGoal goal;

        goal.target_pose.header.frame_id = "/base_link";
        goal.target_pose.header.stamp = ros::Time::now();

        goal.target_pose.pose.position.x = stamped_good_rg.point.x;
        goal.target_pose.pose.position.y = stamped_good_rg.point.y;
        //ROS_INFO("angle: %f", selected_orientation.z);
        
        tf2::Quaternion new_orientation;
        new_orientation.setRPY(0,0,selected_orientation.z);

        goal.target_pose.pose.orientation.x = (double)new_orientation.x();
        goal.target_pose.pose.orientation.y = (double)new_orientation.y();
        goal.target_pose.pose.orientation.z = (double)new_orientation.z();
        goal.target_pose.pose.orientation.w = (double)new_orientation.w();
        //ROS_INFO("%f", goal.target_pose.pose.position.x);
        //ROS_INFO("%f", goal.target_pose.pose.position.y);
        //ROS_INFO("%f", goal.target_pose.pose.orientation.z);
        //sleep(1000);
        mbc.sendGoal(goal);
        last_update = time(NULL);
        //mbc.waitForResult();
        //ROS_INFO("enrico2");

    }

    void heuristics::Knowledge::detectionCallback(const p3at_carto::DoorDetection::ConstPtr& detection_msg){
        //nav_msgs::Odometry tmpPose = detection_msg->odom;
        //Orientation rotation = convert_from_quaternions(tmpPose);
        std::vector<p3at_carto::DoorInfo> tmpDetectionList = detection_msg->detectionList;
        for(int i = 0; i < tmpDetectionList.size(); i++){
            
            p3at_carto::DoorInfo thisDoor = tmpDetectionList[i];
            //float thisDoorAngle = atan2(thisDoor.centerX, thisDoor.centerZ);
            float thisDoorX = thisDoor.centerX;//tmpPose.pose.pose.position.x + thisDoor.centerZ * cos(rotation.z+thisDoorAngle);
            float thisDoorY = thisDoor.centerZ;//tmpPose.pose.pose.position.y + thisDoor.centerX * cos(rotation.z+thisDoorAngle);
            float aperture = thisDoor.aperture;
            bool duplicate = false;
            
            for(int j = 0; j < detectionList.size(); j++){
                SingleDetection comparedDoor = detectionList[j];
                float doorsDistance = sqrt(pow(comparedDoor.doorPose.x-thisDoorX, 2) + pow(comparedDoor.doorPose.y-thisDoorY, 2));
                if(doorsDistance < doors_distance_thresh){
                    comparedDoor.doorPose.x = thisDoorX;
                    comparedDoor.doorPose.y = thisDoorY;
                    comparedDoor.aperture = aperture;
                    time(&comparedDoor.detectionTime);
                    duplicate = true;
                    break;
                }
            }

            if(!duplicate){
                ROS_INFO("New Door");
                SingleDetection newDoor;
                newDoor.doorPose.x = thisDoorX;
                newDoor.doorPose.y = thisDoorY;
                newDoor.aperture = aperture;
                time(&newDoor.detectionTime);
                detectionList.push_back(newDoor);
            }
        }
    }

    void heuristics::Knowledge::mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& map_msg){
        last_map = *map_msg;
    }
}