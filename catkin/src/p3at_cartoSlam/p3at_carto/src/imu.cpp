#include <ros/ros.h>
#include <sensor_msgs/Imu.h>

void imuCallback(const sensor_msgs::Imu::ConstPtr& msg, ros::Publisher pub){
  sensor_msgs::Imu new_msg = *msg;
  new_msg.header.frame_id = "imu_link";
  pub.publish(new_msg);
}

int main(int argc, char** argv){
    ros::init(argc, argv, "imu");
    
    ros::NodeHandle n;
    ros::Publisher pub = n.advertise<sensor_msgs::Imu>("imu",20);
    ros::Subscriber sub = n.subscribe<sensor_msgs::Imu>("/imu/data", 20, boost::bind(imuCallback, _1, pub));
    

    ros::spin();
    return 0;
}

