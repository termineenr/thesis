#include "door_detector.h"

using namespace cv;
using namespace cv::dnn;
using namespace std;
using namespace message_filters;

namespace p3at_cartoSlam{

    typedef sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::PointCloud2> ImgSync;

    struct door_detector::detectInfo{
        void detectCallback(const sensor_msgs::Image::ConstPtr& rgb_msg, const sensor_msgs::Image::ConstPtr& depth_msg, const sensor_msgs::PointCloud2::ConstPtr& points_msg);
        string modelConf;
        string modelBin;
        string classNamesPath;
        string outputVideo;
        dnn::Net net;
        std::vector<String> outputsNames;
        vector<String> classNamesVec;
        yoloTrack tracker;
        //VideoWriter vw;

        std::string door_name;

        ros::Publisher detection_pub;

        message_filters::Subscriber<sensor_msgs::Image> rgb_sub;
        message_filters::Subscriber<sensor_msgs::Image> depth_sub;
        message_filters::Subscriber<sensor_msgs::PointCloud2> points_sub;

        boost::shared_ptr<Synchronizer<ImgSync>> sync_hold;
    };

    vector<String> getOutputsNames(const Net& net)
    {
        static vector<String> names;
        if (names.empty())
        {
            //Get the indices of the output layers, i.e. the layers with unconnected outputs
            vector<int> outLayers = net.getUnconnectedOutLayers();
            
            //get the names of all the layers in the network
            vector<String> layersNames = net.getLayerNames();
            
            // Get the names of the output layers in names
            names.resize(outLayers.size());
            for (size_t i = 0; i < outLayers.size(); ++i){
                names[i] = layersNames[outLayers[i] - 1];
            }
            
        }
        return names;
    }

    Size2f computeObjSize(Point2i center, Size2i size, Mat depth){
        /*float right, left, top, bottom, depth = 0;
        int start_x = center.x - (size.width/2);
        int end_x = center.x + (size.width/2);
        int start_y = center.y - (size.height/2);
        int end_y = center.y + (size.height/2);
        int i = start_x;

        while((isnan(left) || depth == 0) && i <= end_x){
            try{
                pcl::PointXYZRGB cur_point = points.at(i, start_y);
                depth = cur_point.z;
                left = cur_point.x;
            }
            catch(exception e){}
            i++;
        }

        i = end_x;
        depth = 0;
        while((isnan(right) || depth == 0) && i >= start_x){
            try{
                pcl::PointXYZRGB cur_point = points.at(i, start_y);
                depth = cur_point.z;
                right = cur_point.x;
            }
            catch(exception e){}
            i--;
        }

        i = start_y;
        depth = 0;
        while((isnan(top) || depth == 0) && i <= end_y){
            try{
                pcl::PointXYZRGB cur_point = points.at(start_x, i);
                depth = cur_point.z;
                top = cur_point.y;
            }
            catch(exception e){}
            
            i++;
        }

        i = end_y;
        depth = 0;
        while((isnan(bottom) || depth == 0) && i >= start_y){
            try{
                pcl::PointXYZRGB cur_point = points.at(start_x, i);
                depth = cur_point.z;
                bottom = cur_point.y;
            }
            catch(exception e){}
            
            i--;
        }

        float estimate_w = std::abs(right - left);
        float estimate_h = std::abs(top - bottom);

        return Size2f(estimate_w,estimate_h);*/
        double min,max;
        minMaxLoc(depth, &min, &max);
        //cout<<min<<" "<<max<<endl;
        const float FL = 3.657;
        const float SENSOR_H = 5.4;
        const float SENSOR_W = 9.6;
        float distance = 0;
        int count = 0;
        for(int i = -(size.height/2); i < (size.height/2); i++){
            for(int j = -(size.width/2); j < size.width/2; j++){
                if((float)depth.at<u_int16_t>(center.x + j,center.y + i) > 200 && (float)depth.at<u_int16_t>(center.x + j,center.y + i) < 6000){
                    distance += (float)depth.at<u_int16_t>(center.x + j,center.y +i);
                    count ++;
                } 
            }
        }
        
        distance = distance/count;
        //cout<<"distance"<<distance<<endl;
        float estimate_h = (size.height * SENSOR_H * distance) / (FL * depth.rows * 1000);
        float estimate_w = (size.width * SENSOR_W * distance) / (FL * depth.cols * 1000);
        return Size2f(estimate_w,estimate_h);
    }

    vector<Point2f> computeAperturePoints(pcl::PointCloud<pcl::PointXYZRGB>& points, Point start, Point end, float slope){
        int i = start.x;
        int j = start.y;
        
        int i_increment, j_increment;
        float angle = atan(slope) * 180/M_PI;
        if(angle < 35){
            i_increment = 2;
            j_increment = 1;
        }
        else if (angle < 55){
            i_increment = 1;
            j_increment = 1;
        }
        else{
            i_increment = 1;
            j_increment = 2;
        }
        ROS_INFO("Angle: %f", angle);
        

        Point2f left_side(0,0);
        Point2f right_side(0,0);
        float front_depth = -1;
        pcl::PointXYZRGB prev_point;

        while(i < end.x && j < end.y){
            pcl::PointXYZRGB cur_point = points.at(i,j);

            if(cur_point.z != 0 && !isnan(cur_point.z)){
                if(front_depth == -1){
                    front_depth = cur_point.z;
                }
                if(std::abs(front_depth-cur_point.z) > 0.6){
                    left_side.x = prev_point.x;
                    left_side.y = prev_point.z;
                    i++;
                    break;
                }

                prev_point = cur_point;
            }
            
            i = i + i_increment;
            j = j + j_increment;
        }
        ROS_INFO("LEFT: (%f,%f)", left_side.x, left_side.y);

        while(i < end.x && j < end.y){
            pcl::PointXYZRGB cur_point = points.at(i,j);
            if(cur_point.z != 0 && !isnan(cur_point.z)){
                if(std::abs(front_depth-cur_point.z) < 0.5){
                    right_side.x = cur_point.x;
                    right_side.y = cur_point.z;
                    break;
                }
            }
            i++;
        }
        ROS_INFO("RIGHT: (%f,%f)", right_side.x, right_side.y);
        std::vector<Point2f> aperturePoints;
        aperturePoints.push_back(left_side);
        aperturePoints.push_back(right_side);
        return aperturePoints;
    }

    void door_detector::detectInfo::detectCallback(const sensor_msgs::Image::ConstPtr& rgb_msg, const sensor_msgs::Image::ConstPtr& depth_msg, const sensor_msgs::PointCloud2::ConstPtr& points_msg){
        //ROS_INFO("DETECTION");
        /*tf::TransformListener listener;
        bool tf_found = false;
        tf::StampedTransform odom;
        while(!tf_found){
            try{
                listener.lookupTransform("/odom", "/base_link", ros::Time(0), odom);
                tf_found = true;
            }
            catch(tf::TransformException& ex){}
        }

        tf::Vector3 odom_trans = odom.getOrigin();
        tf::Quaternion odom_rot = odom.getRotation();*/
        
        cv_bridge::CvImageConstPtr cv_rgb_ptr;
        cv_bridge::CvImageConstPtr cv_depth_ptr;

        cv_rgb_ptr = cv_bridge::toCvShare(rgb_msg, rgb_msg->encoding);
        cv_depth_ptr = cv_bridge::toCvShare(depth_msg, depth_msg->encoding);

        Mat frame, depth;
        cv_rgb_ptr->image.copyTo(frame);
        cv_depth_ptr->image.copyTo(depth);
        /*sensor_msgs::Image temp;
        if(depth_msg->encoding == "16UC1"){
            temp.header = depth_msg->header;
            temp.height = depth_msg->height;
            temp.width = depth_msg->width;
            temp.is_bigendian = depth_msg->is_bigendian;
            temp.step = depth_msg->step;
            temp.data = depth_msg->data;
            temp.encoding = "mono16";
        }

        try{
            cv_rgb_ptr = cv_bridge::toCvCopy(rgb_msg, sensor_msgs::image_encodings::BGR8);
            cv_depth_ptr = cv_bridge::toCvCopy(temp, sensor_msgs::image_encodings::MONO16);
        }
        catch (cv_bridge::Exception& e){
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        Mat frame = cv_rgb_ptr->image;
        Mat depth = cv_depth_ptr->image;*/
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr points(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::fromROSMsg(*points_msg, *points);
        
        if (frame.empty()){
            return;
        }
        
        if (frame.channels() == 4)
            cvtColor(frame, frame, COLOR_BGRA2BGR);

        if((frame.cols > depth.cols) || (frame.rows > depth.rows)){
            int x = (frame.cols - depth.cols)/2;
            int y = (frame.rows - depth.rows)/2;
            Rect roi(x , y , depth.cols, depth.rows);
            //cout<<roi<<endl;
            frame = frame(roi);
        }

        Mat inputBlob = blobFromImage(frame, 1 / 255.F, Size(416, 416), Scalar(0,0,0), true, false); //Convert Mat to batch of images
        net.setInput(inputBlob);                   //set the network input
        vector<Mat> outs;
        net.forward(outs, outputsNames);   //compute output
        vector<Door> detectedDoors;
        /*for(int i = 0; i < tracker.doorList.size(); i++){
            tracker.doorList[i].decreaseLife();
        }*/
        
        for(int k = 0; k < outs.size(); k++){
            Mat detectionMat = outs[k];
            vector<double> layersTimings;
            double tick_freq = getTickFrequency();
            double time_ms = net.getPerfProfile(layersTimings) / tick_freq * 1000;
            putText(frame, format("FPS: %.2f ; time: %.2f ms", 1000.f / time_ms, time_ms),
                    Point(20, 20), 0, 0.5, Scalar(0, 0, 255));
            float confidenceThreshold = 0.25;

            for (int i = 0; i < detectionMat.rows; i++)
            {
                /*const int probability_index = 0;
                const int probability_size = detectionMat.cols - probability_index;
                float *prob_array_ptr = &detectionMat.at<float>(i, probability_index);
                size_t objectClass = max_element(prob_array_ptr, prob_array_ptr + probability_size) - prob_array_ptr;
                float confidence = detectionMat.at<float>(i, (int)objectClass + probability_index);*/
                Mat scores = detectionMat.row(i).colRange(5, detectionMat.cols);
                //cout<<scores<<endl;
                Point classIdPoint;
                double confidence;
                // Get the value and location of the maximum score
                minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
                if (confidence > confidenceThreshold)
                {
                    float x_center = detectionMat.at<float>(i, 0) * frame.cols;
                    float y_center = detectionMat.at<float>(i, 1) * frame.rows;
                    float width = detectionMat.at<float>(i, 2) * frame.cols;
                    float height = detectionMat.at<float>(i, 3) * frame.rows;

                    Point2i center(cvRound(x_center),cvRound(y_center));
                    Size2i size(cvRound(width),cvRound(height));
                    Point2i admissible_coord((frame.cols/3), (frame.rows/10));
                    if(center.x > admissible_coord.x && center.x < (frame.cols-admissible_coord.x)){
                        if(center.y > admissible_coord.y && center.y < (frame.rows-admissible_coord.y)){
                            Size2f estimate_real = computeObjSize(center, size, depth);
                            //cout<<estimate_real<<endl;
                            
                            if((estimate_real.height > 1.0 && estimate_real.height < 3.5) /*&& (estimate_real.width > 0.3 && estimate_real.width< 2.0)*/){
                                Rect2d bbox(center.x, center.y, size.width, size.height);
                                Door d(frame, bbox);
                                detectedDoors.push_back(d);
                                
                                
                                /*Point p1(cvRound(x_center - width / 2), cvRound(y_center - height / 2));
                                Point p2(cvRound(x_center + width / 2), cvRound(y_center + height / 2));
                                Rect object(p1, p2);
                                Scalar object_roi_color(0, 255, 0);
                                int objectClass = classIdPoint.x;
                                
                                DOPO rectangle(frame, object, object_roi_color);
                                
                                //rectangle(depth, object, object_roi_color);
                                
                                String className = objectClass < classNamesVec.size() ? classNamesVec[objectClass] : cv::format("unknown(%d)", objectClass);
                                String label = format("%s: %.2f", className.c_str(), confidence);
                                int baseLine = 0;
                                Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
                                rectangle(frame, Rect(p1, Size(labelSize.width, labelSize.height + baseLine)),
                                        object_roi_color, FILLED);
                                putText(frame, label, p1 + Point(0, labelSize.height),
                                        FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0));*/
                            }
                        }
                    }
                    
                    
                    /*try{
                        cvtColor(depth, depth, CV_GRAY2BGR);
                        depth.convertTo(depth, CV_8UC3);
                        cout<<"conversion"<<endl;
                        rectangle(depth, Rect(p1, Size(labelSize.width, labelSize.height + baseLine)),
                            object_roi_color, FILLED);
                        putText(depth, label, p1 + Point(0, labelSize.height),
                            FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0));
                    }
                    catch(exception e){}*/
                }
            }
        }
        
        tracker.setCurFrame(frame);
        tracker.trackAll();

        for(int i = 0; i < detectedDoors.size(); i++){
            Door door = detectedDoors[i];
            int position = tracker.isInList(door);
            if(position == -1){
                tracker.trackDoors.push_back(door);
            }
            else{
                Door trackDoor = tracker.trackDoors[position];
                Rect dBbox = door.getBbox();
                Rect tdBbox = trackDoor.getBbox();
                int x = (int) ((2 * dBbox.x + tdBbox.x) / 3);
                int y = (int) ((2 * dBbox.y + tdBbox.y) / 3);
                int width = (int) ((2 * dBbox.width + tdBbox.width) / 3);
                int height = (int) ((2 * dBbox.height + tdBbox.height) / 3);
                Rect2d newBbox(x, y, width, height);
                tracker.trackDoors[position].setBbox(newBbox);
                tracker.trackDoors[position].numDetection++;
                tracker.trackDoors[position].substituteTracker();
            }
        }

        for(int i = 0; i < tracker.trackDoors.size(); i++){
            if(tracker.trackDoors[i].toVisualize){
                if((tracker.trackDoors[i].numDetection/(tracker.trackDoors[i].numFrames * 1.0)) > 0.15){
                    Rect2d imageRect(Point2d(0,0), Point2d(frame.cols,frame.rows));
                    Rect2d dBbox = tracker.trackDoors[i].getBbox();
                    Rect2d boxInImg = dBbox & imageRect;
                    //cout<<imageRect<<", "<<dBbox<<", "<<boxInImg<<endl;
                    if((boxInImg.width*boxInImg.height)/(dBbox.width*dBbox.height) > 0.5){
                        Point p1(cvRound(dBbox.x - dBbox.width / 2), cvRound(dBbox.y - dBbox.height / 2));
                        Point p2(cvRound(dBbox.x + dBbox.width / 2), cvRound(dBbox.y + dBbox.height / 2));
                        Rect box(p1,p2);
                        Scalar color(0,255,0);
                        rectangle(frame, box, color, 4);
                        String name = "Door";
                        int baseLine = 0;
                        Size labelSize = getTextSize(name, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
                        rectangle(frame, Rect(p1, Size(labelSize.width, labelSize.height + baseLine)), color, FILLED);
                        putText(frame, name, p1 + Point(0, labelSize.height), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0));    
                    }
                }
                else{
                    tracker.trackDoors.erase(tracker.trackDoors.begin()+i);
                }
                
            }
        }

        p3at_carto::DoorDetection detection_msg;
        std::vector<p3at_carto::DoorInfo> single_detections_list;
        //tf::TransformListener tf_listener;

        for(int t = 0; t < tracker.trackDoors.size(); t++){
            Door door = tracker.trackDoors[t];
            if(door.toVisualize && !door.apertureComputed){
                p3at_carto::DoorInfo single_detection_msg;
                Rect2d dBbox = door.getBbox();

                float slope = (dBbox.y - (dBbox.y - dBbox.height/2))/(dBbox.x - (dBbox.x - dBbox.width/2));

                int startingX = (int) (dBbox.x - 1.4 * dBbox.width/2);
                int endingX = (int) (dBbox.x + 1.4 * dBbox.width/2);
                int startingY = (int) (dBbox.y - 1.4 * dBbox.height/2);
                int endingY = (int) (dBbox.y + 1.4 * dBbox.height/2);

                Point startingP(std::max(startingX, 0), std::max(startingY, 0));
                Point endingP(std::min(endingX, (int)points->width), std::min(endingY, (int)points->height));
                
                std::vector<Point2f> aperturePoints = computeAperturePoints(*points, startingP, endingP, slope);
                Point2f left_point = aperturePoints[0];
                Point2f right_point = aperturePoints[1];
                
                if(!(right_point.x == 0 && right_point.y == 0) || (left_point.x == 0 && left_point.y == 0)){
                    float aperture = sqrt(pow(left_point.x-right_point.x, 2) + pow(left_point.y-right_point.y, 2));
                    float midX = (left_point.x + right_point.x) / 2;
                    float midZ = (left_point.y + right_point.y) / 2;
                    ROS_INFO("Aperture: %f---Center: (%f,%f)", aperture, midX, midZ);             
                    for(int i = 0; i < (*points).width; i++){
                        for(int j = 0; j < (*points).height; j++){
                            if(std::abs((*points).at(i,j).x - midX) < 0.15 && std::abs((*points).at(i,j).z - midZ) < 0.15 && (*points).at(i,j).y > 0.15){
                                (*points).at(i,j).r = 0;
                                (*points).at(i,j).g = 255;
                                (*points).at(i,j).b = 0;
                            }
                        }
                    }
                    time_t now;
                    time(&now);
                    std::stringstream ss;
                    ss << now;
                    string pclname = "/home/enrico/catkin_ws/src/p3at_cartoSlam/p3at_carto/door_pcl/" + ss.str() + ".pcd"; 
                    pcl::io::savePCDFileASCII(pclname, *points);

                    if(aperture > 0.6){
                        bool found_tf = false;
                        tf::TransformListener tf_listener;
                        tf::Vector3 translation;

                        while(!found_tf)
                        {
                            try{
                                tf::StampedTransform stf;
                                tf_listener.lookupTransform("/map", "/kinect2_link",ros::Time(0), stf);

                                //translation = stf.getOrigin();
                                //midX = midX + translation.getY();
                                //midZ = midZ + translation.getX();
                                found_tf = true;
                                ROS_INFO("(%f,%f)", midX, midZ);
                            }
                            catch(tf::TransformException& ex){}
                        }
                        
                        geometry_msgs::PointStamped door_kinect_pose;
                        door_kinect_pose.header.frame_id = "/kinect2_link";
                        door_kinect_pose.header.stamp = ros::Time();
                        door_kinect_pose.point.x = midZ;
                        door_kinect_pose.point.y = midX;
                        door_kinect_pose.point.z = 0;

                        geometry_msgs::PointStamped door_map_pose;
                        door_map_pose.header.frame_id = "/map";
                        tf_listener.transformPoint("/map", door_kinect_pose, door_map_pose);

                        single_detection_msg.centerX = door_map_pose.point.x;
                        single_detection_msg.centerZ = door_map_pose.point.y;
                        ROS_INFO("DOOR_POSE: %f, %f", door_map_pose.point.x, door_map_pose.point.y);
                        single_detection_msg.aperture = aperture;
                        single_detections_list.push_back(single_detection_msg);
                    }
                    tracker.trackDoors[t].apertureComputed = true;
                    //imwrite("/home/enrico/catkin_ws/src/p3at_cartoSlam/p3at_carto/door_det_img/"+door_name, frame);
                }
                
            }
            
        }

        if(!single_detections_list.empty()){
            /*nav_msgs::Odometry odom_msg;
            odom_msg.pose.pose.position.x = odom_trans.getX();
            odom_msg.pose.pose.position.y = odom_trans.getY();
            odom_msg.pose.pose.position.z = odom_trans.getZ();
            odom_msg.pose.pose.orientation.x = odom_rot.getX();
            odom_msg.pose.pose.orientation.y = odom_rot.getY();
            odom_msg.pose.pose.orientation.z = odom_rot.getZ();
            odom_msg.pose.pose.orientation.w = odom_rot.getW();
            detection_msg.odom = odom_msg;*/
            detection_msg.detectionList = single_detections_list;
            detection_pub.publish(detection_msg);
        }
        
        //imshow("YOLO: Detections", frame);
        //imshow("DEPTH", depth);
        //vw << frame;        
    }

    door_detector::door_detector(ros::NodeHandle* nh, ros::NodeHandle* nh_params){
        info = new detectInfo;

        string depth_top;
        string img_top;
        string points_top;

        nh_params->param<string>("ModelConf", info->modelConf, string("/home/enrico/darknet/cfg/tiny_doorv3.cfg"));
        nh_params->param<string>("ModelBin", info->modelBin, string("/home/enrico/darknet/backup/yolov3-tiny.backup"));
        nh_params->param<string>("NamesPath", info->classNamesPath, string("/home/enrico/darknet/data/door.names"));
        nh_params->param<string>("door_name", info->door_name, string("door.jpg"));
        nh_params->param<string>("RGBTopic", img_top, string("rgb_img"));
        nh_params->param<string>("DepthTopic", depth_top, string("depth_img"));
        nh_params->param<string>("PointsTopic", points_top, string("points_img"));

        info->net = readNetFromDarknet(info->modelConf, info->modelBin);
        info->net.setPreferableTarget(DNN_TARGET_OPENCL);
        
        ifstream classNamesFile(info->classNamesPath);
        if (classNamesFile.is_open())
        {
            string className = "";
            while (std::getline(classNamesFile, className))
                info->classNamesVec.push_back(className);
        }
        
        classNamesFile.close();
        info->outputsNames = getOutputsNames(info->net);
        
        info->tracker = yoloTrack();

        //info->vw.open(info->outputVideo, VideoWriter::fourcc('M', 'P', 'E', 'G'), 15, Size(960,540), true);
        
        info->detection_pub = nh->advertise<p3at_carto::DoorDetection>("/door_detection", 10);

        info->rgb_sub.subscribe(*nh, img_top, 1);
        info->depth_sub.subscribe(*nh, depth_top, 1);
        info->points_sub.subscribe(*nh, points_top, 1);
        info->sync_hold.reset(new Synchronizer<ImgSync>(ImgSync(10), info->rgb_sub, info->depth_sub, info->points_sub));
        info->sync_hold->registerCallback(boost::bind(&door_detector::detectInfo::detectCallback, info, _1, _2, _3));
        ROS_INFO("STARTING DETECTION");
        
    }
}
