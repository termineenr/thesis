#include "yoloTrack.h"

using namespace cv;
using namespace std;

yoloTrack::yoloTrack(){}

void yoloTrack::setCurFrame(Mat frame){
    for(int i = 0; i < trackDoors.size(); i++){
        trackDoors[i].setFrame(frame);
    }
}

void yoloTrack::trackAll(){
    for(int i = 0; i < trackDoors.size(); i++){
        trackDoors[i].updateTracker();
    }
}

int yoloTrack::isInList(Door door){
    for(int i = 0; i < trackDoors.size(); i++){
        Rect dBbox = door.getBbox();
        Rect tdBbox = trackDoors[i].getBbox();

        Rect overlappingRect = dBbox & tdBbox;
        double overlappingArea = overlappingRect.width * overlappingRect.height;
        double dBboxArea = dBbox.width * dBbox.height;
        double tdBboxArea = tdBbox.width * tdBbox.height;
        if((overlappingArea/min(dBboxArea, tdBboxArea)) > 0.3){
            return i;
        }
    }
    return -1;
}