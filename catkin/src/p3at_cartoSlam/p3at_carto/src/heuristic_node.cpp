#include "heuristics.h"

int main(int argc, char** argv){
    ros::init(argc, argv, "p3at_heur");

    ros::NodeHandle nh(""), nh_param("~");
    p3at_cartoSlam::heuristics heur(&nh, &nh_param);

    ros::Rate r(5);
    while(ros::ok()){
        ros::spinOnce();
        r.sleep();
    }
    
}