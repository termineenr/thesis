-- Copyright 2016 The Cartographer Authors
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

include "p3at_2dl.lua"

TRAJECTORY_BUILDER.pure_localization = true

TRAJECTORY_BUILDER_2D.submaps.num_range_data = 90
--TRAJECTORY_BUILDER_2D.submaps.grid_options_2d.resolution = 0.15
--TRAJECTORY_BUILDER_2D.voxel_filter_size = 0.1
TRAJECTORY_BUILDER_2D.max_range = 13
POSE_GRAPH.global_sampling_ratio = 0.1
POSE_GRAPH.constraint_builder.sampling_ratio = 0.01
POSE_GRAPH.optimize_every_n_nodes = 1
POSE_GRAPH.constraint_builder.min_score = 0.5
POSE_GRAPH.constraint_builder.global_localization_min_score = 0.5
--POSE_GRAPH.global_constraint_search_after_n_seconds = 20
--POSE_GRAPH.constraint_builder.ceres_scan_matcher.ceres_solver_options.max_num_iterations = 3
--POSE_GRAPH.constraint_builder.max_constraint_distance = 100
POSE_GRAPH.constraint_builder.fast_correlative_scan_matcher.linear_search_window = 7
--TRAJECTORY_BUILDER_2D.use_online_correlative_scan_matching = true
--TRAJECTORY_BUILDER_2D.real_time_correlative_scan_matcher.linear_search_window = 1

return options
